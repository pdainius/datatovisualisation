from base.base_trainer import BaseTrain
from tqdm import tqdm
import numpy as np
import logging
import os
import glob
import tensorflow as tf


class Trainer(BaseTrain):
    def __init__(self, model, config, sess, data):
        super(Trainer, self).__init__(model,
                                      config,
                                      sess,
                                      data)

    def train_model(self, placeholders):
        prediction_outcome = self.sess.run(self.model,
                                           feed_dict={
                                                placeholders['x_data_train']: self.data['Xtr'],
                                                placeholders['x_data_test']: self.data['Xtest'],
                                                placeholders['y_data_train']: self.data['Ytr']
                                            })

        # evaluation
        accuracy = 0
        for pred, actual in zip(prediction_outcome, self.data['Ytest']):
            if pred == np.argmax(actual):
                accuracy += 1

        print("\nAccuracy: {}".format(accuracy / len(prediction_outcome)))