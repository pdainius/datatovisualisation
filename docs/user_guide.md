# 2.1 Installation

Here is a step by step guide on how to install Data To Visualisation project. It will get you to a point of having a local running instance.

## 2.2 Requirements

First, obtain [Python 3.6](https://www.python.org/) and [virtualenv](https://docs.python-guide.org/dev/virtualenvs/) if you do not already have them. Using a virtual environment is strongly recommended, since it will help you to avoid clutter in your system-wide libraries.

Additionally this project depends on:

* [Tensorflow](https://www.tensorflow.org/) (version >=1.13.1)
* [PyYAML](https://pyyaml.org/) (version >=3.13)
* [Numpy](https://www.numpy.org) (version >=1.16.3)

## 2.3 Get and run

Clone the repository somewhere on your disk and enter to the repository:

```
git clone https://pdainius@bitbucket.org/pdainius/datatovisualisation.git
cd datatovisualisation
```

Create a virtual environment and activate it:

```
virtualenv venv
source venv/bin/activate
```

Next, install the dependencies using `pip` (make sure you are inside of the virtual environment):

```
pip install -r requirements.txt
```

###Run

To run or train the neural network use this command:
```
python3 main.py -c --config [path to config] -a --action [train(default)|run]
```

## 2.4 Datasets

Datasets should be stored inside data folder with a folder name that would determine the category of the files. 

```
data/[Category_Name]/csv/*.csv
data/[Category_Name]/json/*.json
```

Also write the Category label_id inside:
```
data/data_labels.json
```
## 2.5 Model 

This particular model uses kNN(k nearest neighbors) algorithm.

![kNN](https://s3-ap-south-1.amazonaws.com/av-blog-media/wp-content/uploads/2018/03/knn3.png)

All of the procedures can be separated into three points. Data preparation, algorithm writing, training. To say precisely, kNN doesn't have the concept of model to train. So you can understand "training" as prediction.

```
feature_number = len(x_vals_train[0]) #Number of features

k = 5 #Number of nearest points that a data point must have

#tensorflow placeholders
x_data_train = tf.placeholder(shape=[None, feature_number], dtype=tf.float32)
y_data_train = tf.placeholder(shape=[None, len(y_vals[0])], dtype=tf.float32)
x_data_test = tf.placeholder(shape=[None, feature_number], dtype=tf.float32)

# manhattan distance
distance = tf.reduce_sum(tf.abs(tf.subtract(x_data_train,tf.expand_dims(x_data_test,1))), axis=2)

# nearest k points
_, top_k_indices = tf.nn.top_k(tf.negative(distance), k=k)
top_k_label = tf.gather(y_data_train, top_k_indices)

sum_up_predictions = tf.reduce_sum(top_k_label, axis=1)
prediction = tf.argmax(sum_up_predictions, axis=1)
```

On TensorFlow, we usually set Variable and placeholder. Variable is for parameters to update and placeholder is for data. This time, kNN doesn’t have parameters to update. So, only placeholder is necessary for train and test data. 
