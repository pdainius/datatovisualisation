from base.base_model import BaseModel
import tensorflow as tf
from tensorflow import keras as keras
import matplotlib.pyplot as plt


class Model(BaseModel):

    def __init__(self, config, data):
        super(Model, self).__init__(config, data)

    def placeholders(self):
        x_data_train = tf.placeholder(shape=[None, self.feature_number], dtype=tf.float32)
        y_data_train = tf.placeholder(shape=[None, len(self.data['Yvals'][0])], dtype=tf.float32)
        x_data_test = tf.placeholder(shape=[None, self.feature_number], dtype=tf.float32)

        d = {
            'x_data_train': x_data_train,
            'y_data_train': y_data_train,
            'x_data_test': x_data_test
        }

        return d

    def build_model(self):
        self.feature_number = len(self.data['Xtr'][0])
        k = 5 # nearest k points

        placeholder = self.placeholders()

        # manhattan distance
        distance = tf.reduce_sum(tf.abs(tf.subtract(placeholder['x_data_train'], 
                                                    tf.expand_dims(placeholder['x_data_test'], 
                                                    1))), 
                                axis=2)

        _, top_k_indices = tf.nn.top_k(tf.negative(distance), k=k)
        top_k_label = tf.gather(placeholder['y_data_train'], top_k_indices)

        sum_up_predictions = tf.reduce_sum(top_k_label, axis=1)
        prediction = tf.argmax(sum_up_predictions, axis=1)

        return prediction, placeholder
