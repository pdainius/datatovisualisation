from data_loader.data_generator import DataGenerator
from data_loader.prepare_data import prepare_data
from models.model import Model
from trainers.trainer import Trainer
from utils.args import get_args
from utils.config import process_config
import logging.config
import json
import tensorflow as tf


def run(args, config):
    pass


def train(args, config):
    # create tensorflow session
    sess = tf.Session()

    # transforms csv/json data to tfrecords
    DataGenerator()

    data = prepare_data()

    # create an instance of the model you want
    model = Model(config, data)
    mod, placeholders = model.build_model()

    # create trainer and pass all the previous components to it
    trainer = Trainer(config=config,
                      model=mod,
                      sess=sess,
                      data=data) 
    
    trainer.train_model(placeholders)
    """
    for example in tf.python_io.tf_record_iterator("data/{0}.tfrecords".format("Category")):
        result = tf.train.Example.FromString(example)
        print(result)
    """


if __name__ == '__main__':
    logging.config.fileConfig('logs/logging.conf')

    # capture the config path from the run arguments
    # then process the json configuration file
    try:
        args = get_args()
        config = process_config(args.config)
    except Exception as e:
        logging.error(e)
        exit(0)

    if args.action == 'train':
        train(args, config)
    else:
        run(args, config)
