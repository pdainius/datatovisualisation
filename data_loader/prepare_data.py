import numpy as np
from sklearn import datasets

def prepare_data():
    """
    Change everything so it would use
    tfrecord instead of some random dataset
    """
    iris = datasets.load_iris()
    x_vals = np.array([x[0:4] for x in iris.data])
    y_vals = np.array(iris.target)

    y_vals = np.eye(len(set(y_vals)))[y_vals]

    x_vals = (x_vals - x_vals.min(0)) / x_vals.ptp(0)

    np.random.seed(59)
    train_indices = np.random.choice(len(x_vals), round(len(x_vals) * 0.8), replace=False)
    test_indices =np.array(list(set(range(len(x_vals))) - set(train_indices)))

    x_vals_train = x_vals[train_indices]
    x_vals_test = x_vals[test_indices]
    y_vals_train = y_vals[train_indices]
    y_vals_test = y_vals[test_indices]

    data = {
        'Xtr': x_vals_train,
        'Xtest': x_vals_test,
        'Ytr': y_vals_train,
        'Ytest': y_vals_test,
        'Yvals': y_vals
    }

    return data