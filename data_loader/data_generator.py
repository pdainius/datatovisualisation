import os
import glob
import tensorflow as tf
import logging
import json
from .csv_to_tfrecords import csv_to_tfrecords
from .json_to_tfrecords import json_to_tfrecords


class DataGenerator(object):
    data_labels_path = 'data/data_labels.json'

    """docstring for DataGenerator"""

    def __init__(self):
        super(DataGenerator, self).__init__()
        self.find_files()

    def find_files(self):
        with open(self.data_labels_path) as json_file:
            data = json.load(json_file)
            for d in data:
                cat = os.getcwd() + "/data/%s" % d
                self.generate_tfrecords(d, cat,data[d])

    """
    Gets csv files inside a Category directory
    """

    def generate_tfrecords(self, label, category_path, label_id):
        csv_files_path = "%s/csv/*.csv" % (category_path)
        json_files_path = "%s/json/*.json" % (category_path)

        csv_files = glob.glob(csv_files_path)
        json_files = glob.glob(json_files_path)
        outfile = os.getcwd() + "/data/{0}.tfrecords".format(label)

        """
        outfile = output file
        Delete tfrecords if exists
        """
        if os.path.exists(outfile):
            os.remove(outfile)

        with tf.python_io.TFRecordWriter(outfile) as writer:
            csv_to_tfrecords(csv_files,
                             writer,
                             label,
                             label_id)
            json_to_tfrecords(json_files,
                              writer,
                              label_id)
