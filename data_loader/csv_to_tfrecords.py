"""
Transforms CSV files to tfrecords format files.
"""
import tensorflow as tf
import pandas as pd
from tqdm import tqdm
import ntpath
import logging

def csv_to_tfrecords(infiles, writer, label_name, label_id):
    """
    using tqdm for some fancy progress bars
    """
    pbar = tqdm(infiles, desc="Transforming CSV to tfrecords", unit="files")
    for file in pbar:
        filename = ntpath.basename(file)
        pbar.set_postfix(file=filename, refresh=False)
        pbar.update(len(filename))

        try:
            rows = pd.read_csv(file, header=0)
            header = pd.read_csv(file, nrows=0).columns.tolist()
        except Exception as e:
            logging.error(e)
            continue

        pbar_rows = tqdm(rows.values, desc=filename)

        """
        Go through all of the rows in csv
        """
        for row in pbar_rows:
            data = {
                'label': tf.train.Feature(
                            int64_list=tf.train.Int64List(
                                value=[label_id]
                                )
                            ),
                'label_text': tf.train.Feature(
                                bytes_list=tf.train.BytesList(
                                    value=[label_name.encode('utf-8')]
                                    )
                                )
            }
            for c, r in enumerate(row):
                """
                Determine what feature type to use
                int - int64_list
                float - float_list
                other - bytes_list
                """
                if isinstance(r, int):
                    data[header[c]] = tf.train.Feature(
                                        int64_list=tf.train.Int64List(
                                            value=[r]))
                elif isinstance(r, float):
                    data[header[c]] = tf.train.Feature(
                                        float_list=tf.train.FloatList(
                                            value=[r]
                                        )
                                    )
                else:
                    try:
                        data[header[c]] = tf.train.Feature(
                                            bytes_list=tf.train.BytesList(
                                                value=[r.encode('utf-8')]))
                    except:
                        continue

            example = tf.train.Example(
                        features=tf.train.Features(
                            feature=data
                        )
                    )
            
            writer.write(example.SerializeToString())
