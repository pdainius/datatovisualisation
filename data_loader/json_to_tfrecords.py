"""
Transforms JSON files to tfrecords format files.
"""

import tensorflow as tf
import pandas as pd
from tqdm import tqdm
import ntpath

"""
Adds json data to a list
"""
def json_to_list(data, headers):
    d = list()
    for header in headers:
        d.append(str(data[header]))

    return d

def json_to_tfrecords(infiles, writer, label_id):
    """
    using tqdm for some fancy progress bars
    """
    pbar = tqdm(infiles, desc="Transforming JSON to tfrecords", unit="files")
    for file in pbar:
        filename = ntpath.basename(file)
        pbar.set_postfix(file=filename, refresh=False)
        pbar.update(len(filename))

        rj = pd.read_json(file)
        df = pd.DataFrame(rj)

        headers = df.columns.values

        for index, row in df.iterrows():
            d = json_to_list(row, headers)

            features = [tf.compat.as_bytes(x) for x in d]

            example = tf.train.Example()
            example.features.feature["features"].bytes_list.value.extend(features)
            example.features.feature["label"].bytes_list.value.extend(label_id)

            writer.write(example.SerializeToString())