import yaml
from bunch import Bunch
import os
import logging


def get_config_from_yml(yml_file):
    """
    Get the config from a yml file
    :param yml_file:
    :return: config(namespace) or config(dictionary)
    """
    # parse the configurations from the config yml file provided
    with open('configs/' + yml_file, 'r') as stream:
        try:
            config_dict = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            logging.error(exc)
            exit(0)

    # convert the dictionary to a namespace using bunch lib
    config = Bunch(config_dict)

    return config, config_dict


def process_config(yml_file):
    config, _ = get_config_from_yml(yml_file)
    config.summary_dir = os.path.join(
        "/experiments", config.exp_name, "summary/")
    config.checkpoint_dir = os.path.join(
        "/experiments", config.exp_name, "checkpoint/")
    return config
