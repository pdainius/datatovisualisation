import tensorflow as tf


class BaseTrain:
    def __init__(self, model, config, sess, data):
        self.model = model
        self.config = config
        self.sess = sess
        self.data = data

    def mnist_generator(self, filepath):
        """
        Keras generator that yields batches from the speicfied tfrecord filename(s)
        """
        raise NotImplementedError
