# Data to Visualization

Machine Learning project that takes a dataset and outputs a possible visualization solution.

## Built With

* [TensorFlow](https://www.tensorflow.org/) - Used for machine learning application

## Authors

* **Dainius Preimantas** - *Initial work*
* **Laura Balčiūnaitė** - *Initial work*
* **Jovydas Dundulis** - *Initial work*
* **Mindaugas Vasiljevas** - *Coordinator*